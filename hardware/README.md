Requirements
--

* compatible with firmware available on [git](https://github.com/candle-usb/candleLight_fw)
* STM32F072 
* RX and TX indicator LEDs

* CAN 
* USB
* reset button

* jumper/button on BOOT0 pin to create possibility of flashing per DFU over USB
